package com.dz.ainur;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

public class DbDataGen {
    public static final String name = "perfomer_";

    public static String getDate(Random r) {
        int year;
        int month;
        int day;
        year = 1995 + r.nextInt(20);
        month = 1 + r.nextInt(12);
        day = 1 + r.nextInt(30);
        return year + "." + month + "." + day;
    }

    public static void main(String[] args) {
        Random r = new Random();
        String startDate;
        String endDate;
        String perfomer;
        String activity;
        for (int i = 0; i < 30; i++) {
            startDate = getDate(r);
            endDate = getDate(r);
            perfomer = name + r.nextInt(20);
            activity = RandomStringUtils.randomAlphabetic(20);
            System.out.println("('" + startDate + "','" + endDate + "','" + perfomer + "','" + activity + "'),");
        }
    }
}
