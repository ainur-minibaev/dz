package com.dz.ainur;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public class DateFormatTest {
    public static void main(String[] args) {
        try {
            System.out.println(DateFormat.getDateInstance().parse("01.01.1900"));
        } catch (ParseException e) {
            e.printStackTrace();
        }


            System.out.println(DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US).format(new Date()));
        try {
            System.out.println(DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US).parse("Feb 2, 2014"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
