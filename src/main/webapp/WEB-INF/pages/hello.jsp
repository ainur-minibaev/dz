<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <%@page contentType="text/html; charset=UTF-8" %>

    <script type="text/javascript" src="<c:url value="/resources/code.js"/>"></script>
</head>
<body>
<div class="content">
    <form:form action="/" method="post" modelAttribute="form">
        <div class="inputs">
            <form:input path="startDate" id="startDate"/>
            <form:errors path="startDate" cssclass="error"/>
            <br>
            <form:input path="endDate" cssclass="error" id="endDate"/>
            <form:errors path="endDate" cssclass="error"/>
            <br>

        </div>

        <div class="select_div">
            <form:select path="performer">
                <form:option value="All performers" label="All performers"/>
                <c:forEach var="item" items="${performers}">
                    <form:option value="${item}" label="${item}"/>
                </c:forEach>
            </form:select>
            <br/>
            <select name="range" id="range" onchange="change()">
                <option value="0"></option>
                <option value="1">Last Qtr</option>
                <option value="2">Last Month</option>
                <option value="3">Last Calendar Year</option>
                <option value="4">Current Year to Date</option>
                <option value="5">Current Qtr to Date</option>
                <option value="6">Current Month to Date</option>
            </select>
            <br>
        </div>

        <button type="submit" class="do">Submit</button>
    </form:form>


    <fmt:setLocale value="en_US" scope="session"/>
    <br>
    <c:if test="${ empty get}">
        <c:choose>
            <c:when test="${ empty reports }">
                <div class="error">
                    <c:out value="Ошибка запроса или пустой ответ"/>
                </div>
            </c:when>
            <c:otherwise>
                <table class="report_table" border="1">
                    <tr>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Performer</th>
                        <th>Activity</th>
                    </tr>
                    <c:forEach var="item" items="${reports}">
                        <tr>
                            <td><fmt:formatDate value="${item.startDate}" pattern="MMM d, yyyy"/></td>
                            <td><fmt:formatDate value="${item.endDate}" pattern="MMM d, yyyy"/></td>
                            <td>${item.performer}</td>
                            <td>${item.activity}</td>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>
    </c:if>

</div>
</body>
</html>