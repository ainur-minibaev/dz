function change() {
    var startF = document.getElementById("startDate");
    var endF = document.getElementById("endDate");
    var selectB = document.getElementById("range");
    var val = selectB.options[selectB.selectedIndex].value;
    var today = new Date();
    var mm = today.getMonth();
    var yy = today.getFullYear();
    if (val == 0) {
        //empty
        setFieldValue(today, startF);
        setFieldValue(today, endF);
    }
    if (val == 1) {
        //last qtr
        var quarter = Math.floor((mm + 3) / 3);
        if (quarter == 1) {
            setQtr(4, yy - 1);
        } else {
            setQtr(quarter - 1, yy);
        }
    }
    if (val == 2) {
        //last month
        var newMonth = mm - 1;
        var newYear = yy;
        if (newMonth == -1) {
            newMonth = 11;
            newYear--;
        }
        setFieldValue(new Date(newYear, newMonth, 1), startF);
        setFieldValue(new Date(newYear, newMonth + 1, 0), endF);
    }
    if (val == 3) {
        //last calendar year
        setFieldValue(new Date(yy - 1, 0, 1), startF);
        setFieldValue(new Date(yy - 1, 11, 31), endF);
    }
    if (val == 4) {
        //current year
        setFieldValue(new Date(yy, 0, 1), startF);
        setFieldValue(new Date(yy, 11, 31), endF);
    }
    if (val == 5) {
        //current qtr
        var quarter = Math.floor((mm + 3) / 3);
        setQtr(quarter, yy);
    }
    if (val == 6) {
        //current month
        setFieldValue(new Date(yy, mm, 1), startF);
        setFieldValue(new Date(yy, mm + 1, 0), endF);
    }


}

function setQtr(qtr, year) {
    var startF = document.getElementById("startDate");
    var endF = document.getElementById("endDate");
    var start = new Date(year, qtr * 3 - 3, 1);
    var end = new Date(start.getFullYear(), start.getMonth() + 3, 0);
    setFieldValue(start, startF);
    setFieldValue(end, endF);
}

function setFieldValue(date, input) {
    var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var dd = date.getDate();
    var mm = date.getMonth();
    var yy = date.getFullYear();
    input.value = month[mm] + ' ' + dd + ", " + yy;
}