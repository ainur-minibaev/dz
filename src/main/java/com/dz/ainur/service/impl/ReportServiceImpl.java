package com.dz.ainur.service.impl;


import com.dz.ainur.QueryForm;
import com.dz.ainur.dao.ReportDAO;
import com.dz.ainur.model.Report;
import com.dz.ainur.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userService")
@Transactional(readOnly = true)
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportDAO reportDAO;


    @Override
    @Transactional
    public List<Report> findAll() {
        return reportDAO.findAll();
    }

    @Override
    @Transactional
    public List<Report> query(QueryForm form) {
        if (form == null) {
            return null;
        }
        return reportDAO.query(form);
    }


    @Override
    @Transactional
    public List<String> getPerformers() {
        return reportDAO.getPerformers();
    }


}
