package com.dz.ainur.service;

import com.dz.ainur.QueryForm;
import com.dz.ainur.model.Report;

import java.util.List;

public interface ReportService {

    List<Report> findAll();

    List<Report> query(QueryForm form);

    List<String> getPerformers();
}
