package com.dz.ainur;

import java.util.Date;

public class QueryForm {
    String startDate;

    String endDate;

    String performer;

    Date start;
    
    Date end;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "QueryForm{" +
                "startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", performer='" + performer + '\'' +
                '}';
    }


}
