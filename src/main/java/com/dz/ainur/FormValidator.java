package com.dz.ainur;


import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Locale;

public class FormValidator implements Validator {

    public static final String WRONG_FORMAT = "Wrong format. Example: Feb 1, 2014";
    public static final String EMPTY_FIELD = "Empty field";

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object o, Errors errors) {
        QueryForm f = (QueryForm) o;
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);
        //empty or whitespace check
        if (f.getStartDate().trim().length() != 0) {
            try {
                f.setStart(formatter.parse(f.getStartDate()));
            } catch (ParseException e) {
                errors.rejectValue("startDate", "", WRONG_FORMAT);
                e.printStackTrace();
                return;
            }
        } else {
            f.setStart(null);
        }
        //empty or whitespace check
        if (f.getEndDate().trim().length() != 0) {
            try {
                f.setEnd(formatter.parse(f.getEndDate()));
            } catch (ParseException e) {
                errors.rejectValue("endDate", "", WRONG_FORMAT);
                e.printStackTrace();
                return;
            }
        } else {
            f.setEnd(null);
        }

    }
}
