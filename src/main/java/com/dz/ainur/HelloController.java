package com.dz.ainur;

import com.dz.ainur.model.Report;
import com.dz.ainur.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/")
public class HelloController {
    public static final String REPORTS = "reports";
    public static final String PERFORMERS = "performers";
    public static final String FORM = "form";
    @Autowired
    ReportService reportService;

    @RequestMapping(method = RequestMethod.GET)
    public String openPage(ModelMap model) {
        //list of all perfomers
        List<String> ps = reportService.getPerformers();
        model.addAttribute(PERFORMERS, ps);
        for (String s : ps) {
            System.out.println(s);
        }
        //empty object for form
        model.addAttribute(FORM, new QueryForm());
        //get label
        model.addAttribute("get", "get");
        return "hello";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String showReports(@ModelAttribute(FORM) QueryForm form, BindingResult result, ModelMap model) {
        model.addAttribute(FORM, form);
        List<String> ps = reportService.getPerformers();
        model.addAttribute(PERFORMERS, ps);
        //form validation, field parse
        new FormValidator().validate(form, result);
        if (result.hasErrors()) {
            //return to page with error
            model.addAttribute(REPORTS, null);
            return "hello";
        }
        //make a query
        List<Report> reports = reportService.query(form);
        if (reports == null || reports.isEmpty()) {
            model.addAttribute(REPORTS, null);
        } else {
            model.addAttribute(REPORTS, reports);
            System.out.println("PRINT ALL REPORTS");
            for (Report r : reports) {
                System.out.println(r);
            }
        }
        return "hello";
    }


}