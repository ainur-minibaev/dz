package com.dz.ainur.dao;

import java.io.Serializable;
import java.util.List;

public interface AbstractDao<E, I extends Serializable> {

    E findById(I id);
    List<E> findAll();
}
