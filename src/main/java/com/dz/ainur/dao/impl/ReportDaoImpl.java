package com.dz.ainur.dao.impl;

import com.dz.ainur.QueryForm;
import com.dz.ainur.dao.ReportDAO;
import com.dz.ainur.model.Report;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReportDaoImpl extends AbstractDaoImpl<Report, String> implements ReportDAO {

    public static final String ALL_PERFORMERS = "All performers";

    protected ReportDaoImpl() {
        super(Report.class);
    }


    @Override
    public List<Report> findAll() {
        return getCurrentSession().createQuery("from reports").list();
    }


    @Override
    public List<Report> query(QueryForm form) {
        Criteria c = getCurrentSession().createCriteria(Report.class);

        //start between start/end date
        if (form.getStart() != null) {
            c.add(Restrictions.ge("startDate", form.getStart()));
        }
        if (form.getEnd() != null) {
            c.add(Restrictions.le("startDate", form.getEnd()));
        }

        //performer restriction
        if (!form.getPerformer().equals(ALL_PERFORMERS)) {
            c.add(Restrictions.eq("performer", form.getPerformer()));
        }
        return c.list();
    }

    @Override
    public List<String> getPerformers() {
        Query q = getCurrentSession().createQuery("SELECT performer from reports GROUP BY performer");
        return q.list();
    }


}
