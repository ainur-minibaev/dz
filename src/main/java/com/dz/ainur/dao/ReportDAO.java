package com.dz.ainur.dao;

import com.dz.ainur.QueryForm;
import com.dz.ainur.model.Report;

import java.util.List;

public interface ReportDAO extends AbstractDao<Report, String> {

    List<Report> query(QueryForm form);

    List<String> getPerformers();
}
