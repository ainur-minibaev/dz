CREATE TABLE reports(
id serial,
start_date date,
end_date date,
performer varchar(255),
activity varchar(255)
);

ALTER TABLE reports
		ADD CONSTRAINT reports_id_pk PRIMARY KEY(id);